package com.control.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "t_record")
@Data
public class RecordEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	/***
	 * 设备id
	 */
	@Column(name="device_id")
	String deviceId;
	/***
	 * 通道id
	 */
	@Column(name="channel_id")
	String channelId;
	
	@Column(name="start_time")
	Integer startTime;
	
	@Column(name="end_time")
	Integer endTime;
	
	@Column(name="file_name")
	String fileName;
	
	@Column(name="file_path")
	String filePath;
	
	@Column(name="folder")
	String folder;
	
	@Column(name="time_len")
	Integer timeLen;
	/***
	 * 访问地址
	 */
	@Column(name="url")
	String url;
	
	@Column(name="file_size")
	Integer fileSize;
}
