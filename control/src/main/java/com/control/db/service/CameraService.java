package com.control.db.service;

import java.util.List;

import com.control.db.entity.CameraEntity;

public interface CameraService {
	CameraEntity saveOrUpdate(CameraEntity cameraEntity);
	CameraEntity findOne(CameraEntity cameraEntity);
	List<CameraEntity> listAll(CameraEntity cameraEntity);
	void delete(CameraEntity cameraEntity);
}
