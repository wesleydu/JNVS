package com.control.db.service;

import java.util.List;

import com.control.db.entity.RecordEntity;

public interface RecordService {
	RecordEntity saveOrUpdate(RecordEntity recordEntity);
	RecordEntity findOne(RecordEntity recordEntity);
	List<RecordEntity> listAll(RecordEntity recordEntity);
}
