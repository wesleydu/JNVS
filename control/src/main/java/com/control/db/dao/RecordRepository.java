package com.control.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.control.db.entity.CameraEntity;
import com.control.db.entity.RecordEntity;

@Repository
public interface RecordRepository extends JpaRepository<RecordEntity, Integer>{
}
